(function($, window, clipTime){
  clipTime.Song = function(data) {
    this.id = data.id.videoId || '';
    this.title = data.snippet.title || '';
    this.description = data.snippet.description || '';
    this.thumbnails = data.snippet.thumbnails || '';
    this.channelTitle = data.snippet.channelTitle || '';
  };

}(jQuery, window, clipTime));
