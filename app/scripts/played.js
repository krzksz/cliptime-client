/**
 * Kolejka odtwarzania, przchowuje informacje o utworach znajdujacych sie
 * w kolejce do bycia odtworzonymi przez odtwarzacz.
 */
(function($, window, clipTime) {

  clipTime.Played = function() {
    var that = this;
    /**
     * Obiekt jQuery kolejki pozwalajacy na manipulacje drzewem DOM kolejki.
     */
    var $played = $('.played');
    /**
     * Prototyp pojedynczego elementu piosenki w drzewie DOM kolejki.
     */
    var $songPrototype = $('<li class="played__song | collection-item avatar">' +
      '<div class="played__song-details">' +
      '<span class="played__song-title"></span>' +
      '<p class="played__song-author"></p></div>' +
      '<a href="#" class="played__song-add | secondary-content"><i class="material-icons">add</i></a>' +
      '</li>');
    /**
     * Prywatna lista utworow.
     */
    var playedList = [];

    var mediator = clipTime.Mediator.getInstance();

    var toggleTimeout = null;
    this.show = function() {
      clearTimeout(toggleTimeout);
      if(!$played.is(':visible')) {
        toggleTimeout = setTimeout(function() {
          $played.fadeIn(200);
        }, 200);
      }
    };

    this.hide = function() {
      clearTimeout(toggleTimeout);
      if($played.is(':visible')) {
        $played.fadeOut(200);
      }
    };

    /**
     * Czysci kolejke z wszystkich utworow.
     */
    this.clear = function(skipRender) {
      playedList = [];

      if (!skipRender) {
        that.renderAll();
      }
    };

    /**
     * Zwraca pierwsza piosenke w kolejce.
     * @return {clipTime.Song|null}    Obiekt piosenki lub null.
     */
    this.getFirst = function() {
      return playedList[0];
    };

    /**
     * Zwraca z kolejki piosenke o podanym id lub null jesli taka nie istnieje.
     * @param  {String} id ID piosenki.
     * @return {clipTime.Song|null}    Obiekt piosenki lub null.
     */
    this.get = function(id) {
      for (var index = 0; index < playedList.length; index++) {
        if (playedList[index].id === id) {
          return playedList[index];
        }
      }

      return null;
    };

    /**
     * Zwraca liste wszystkich piosenek w kolejce.
     * @return {[clipTime.Song]|[]} Lista piosenek lub pusta tablica.
     */
    this.getAll = function() {
      return playedList;
    };

    /**
     * Dodaje podana piosenke na poczatek kolejki.
     * @param  {clipTime.Song} song Piosenka do dodania.
     * @param  {Boolean} skipRender Czy odswiezyc widok HTML kolejki.
     */
    this.prepend = function(song, skipRender) {
      if (!(song instanceof clipTime.Song)) {
        console.error(song, 'Argument is not an instance of clipTime.Song!');
      }
      playedList.unshift(song);

      if (!skipRender) {
        that.renderAll();
      }
    };

    /**
     * Dodaje podana piosenke na koniec kolejki.
     * @param  {clipTime.Song} song Piosenka do dodania.
     * @param  {Boolean} skipRender Czy odswiezyc widok HTML kolejki.
     */
    this.add = function(song, skipRender) {
      if (!(song instanceof clipTime.Song)) {
        console.error(song, 'Argument is not an instance of clipTime.Song!');
      }
      playedList.push(song);

      if (!skipRender) {
        that.renderAll();
      }
    };

    /**
     * Usuwa piosenke o podanym ID z kolejki i zwraca ja.
     * @param  {String} id ID piosenki do usuniecia.
     * @param  {Boolean} skipRender Czy odswiezyc widok HTML kolejki.
     * @return {clipTime.Song|null}    Usunieta piosenka lub null.
     */
    this.remove = function(id, skipRender) {
      for (var index = 0; index < playedList.length; index++) {
        if (playedList[index].id === id) {
          var removedSong = playedList[index];
          playedList.splice(index, 1);

          return removedSong;
        }
      }

      if (!skipRender) {
        that.renderAll();
      }

      return null;
    };

    /**
     * Usuwa pierwszy element z kolejki.
     * @param  {Boolean} skipRender Czy odswiezyc widok HTML kolejki.
     */
    this.shift = function(skipRender) {
      playedList.shift();

      if (!skipRender) {
        that.renderAll();
      }

      return null;
    };

    /**
     * Dodaje wszystkie piosenki z listy do kolejki odtwarzania.
     * @param  {[clipTime.Song]} songs Lista piosenek do dodania.
     */
    this.addAll = function(songs) {
      if (Object.prototype.toString.call(songs) !== '[object Array]') {
        console.error(songs, 'Argument is not an array!');
      }

      songs.forEach(function(index, song) {
        that.add(song, true);
      });

      that.renderAll();
    };

    /**
     * Generuje obiekt jQuery dla podanej piosenki.
     * @param  {clipTime.Song} song Piosenka.
     * @return {jQuery} Obiekt jQuery dla podanej piosenki.
     */
    this.render = function(song) {
      if (!(song instanceof clipTime.Song)) {
        console.error(song, 'Argument is not an instance of clipTime.Song!');
      }

      var $playedElement = $songPrototype.clone();
      $playedElement.attr('data-yt-video-id', song.id);
      $playedElement.find('.played__song-title').text(song.title);
      $playedElement.find('.played__song-author').text(song.channelTitle);

      return $playedElement;
    };

    /**
     * Generuje caly HTML aktualnej kolejki.
     */
    this.renderAll = function() {
      $played.children('.played__song, .played__no-songs').remove();
      if (playedList.length > 0) {
        playedList.forEach(function(song) {
          $played.append(that.render(song));
        });
      } else {
        $played.append('<li class="played__no-songs">No played songs...</li>');
      }
    };

    /**
     * Ładuje wybrane wideo po kliknieciu na nie w liscie kolejki i przesuwa
     * go na jej przod.
     */
    $played.on('click', '.played__song', function(event) {
      var $playedSong = $(this).closest('.played__song');
      var playedSong = that.get($playedSong.attr('data-yt-video-id'));
      if(playedSong) {
        that.remove(playedSong.id);
        mediator.publish('videoLoad', playedSong.id);
        mediator.publish('queuePrepend', playedSong);
      }
    });

    /**
     * Ususwa wybrane wideo z kolejki po kliknieciu na ikone usuwania.
     */
    $played.on('click', '.played__song-add', function(event) {
      event.preventDefault();
      event.stopPropagation();
      var $playedSong = $(this).closest('.played__song');
      var playedSong = that.get($playedSong.attr('data-yt-video-id'));
      mediator.publish('queueAdd', playedSong);
    });

    mediator.subscribe('playedPrepend', this.prepend);
    mediator.subscribe('playedLoad', function() {
      var firstSong = playedList.shift();
      if(firstSong) {
        mediator.publish('videoLoad', firstSong.id);
        mediator.publish('queuePrepend', firstSong);
        that.remove(firstSong.id);
      }
    });
    mediator.subscribe('playedHide', this.hide);
    mediator.subscribe('playedShow', this.show);
  };

}(jQuery, window, clipTime));
