/**
 * Kolejka odtwarzania, przchowuje informacje o utworach znajdujacych sie
 * w kolejce do bycia odtworzonymi przez odtwarzacz.
 */
(function($, window, clipTime) {

  clipTime.Queue = function() {
    var that = this;
    /**
     * Obiekt jQuery kolejki pozwalajacy na manipulacje drzewem DOM kolejki.
     */
    var $queue = $('.queue');
    /**
     * Prototyp pojedynczego elementu piosenki w drzewie DOM kolejki.
     */
    var $songPrototype = $('<li class="queue__song | collection-item avatar">' +
      '<div class="queue__song-details">' +
      '<span class="queue__song-title"></span>' +
      '<p class="queue__song-author"></p></div>' +
      '<a href="#" class="queue__song-delete | secondary-content"><i class="material-icons infinite">delete</i></a>' +
      '</li>');
    /**
     * Prywatna lista utworow.
     */
    var queueList = [];

    var mediator = clipTime.Mediator.getInstance();

    this.loadNext = function() {
      if(queueList.length > 1) {
        that.shift();
        var nextVideo = that.getFirst();
        if(nextVideo) {
          mediator.publish('videoLoad', nextVideo.id);
        }
      }
    };

    var toggleTimeout = null;
    this.toggle = function() {
      clearTimeout(toggleTimeout);
      if($queue.is(':visible')) {
        $queue.fadeOut(200);
      } else {
        toggleTimeout = setTimeout(function() {
          $queue.fadeIn(200);
        }, 200);
      }
    };

    this.show = function() {
      clearTimeout(toggleTimeout);
      if(!$queue.is(':visible')) {
        toggleTimeout = setTimeout(function() {
          $queue.fadeIn(200);
        }, 200);
      }
    };

    this.hide = function() {
      clearTimeout(toggleTimeout);
      if($queue.is(':visible')) {
        $queue.fadeOut(200);
      }
    };

    /**
     * Czysci kolejke z wszystkich utworow.
     */
    this.clear = function(skipRender) {
      queueList = [];

      if (!skipRender) {
        that.renderAll();
      }
    };

    /**
     * Zwraca z kolejki piosenke o podanym id lub null jesli taka nie istnieje.
     * @param  {String} id ID piosenki.
     * @return {clipTime.Song|null}    Obiekt piosenki lub null.
     */
    this.get = function(id) {
      for (var index = 0; index < queueList.length; index++) {
        if (queueList[index].id === id) {
          return queueList[index];
        }
      }

      return null;
    };

    /**
     * Zwraca pierwsza piosenke w kolejce.
     * @return {clipTime.Song|null}    Obiekt piosenki lub null.
     */
    this.getFirst = function() {
      return queueList[0];
    };

    /**
     * Zwraca liste wszystkich piosenek w kolejce.
     * @return {[clipTime.Song]|[]} Lista piosenek lub pusta tablica.
     */
    this.getAll = function() {
      return queueList;
    };

    /**
     * Dodaje podana piosenke na poczatek kolejki.
     * @param  {clipTime.Song} song Piosenka do dodania.
     * @param  {Boolean} skipRender Czy odswiezyc widok HTML kolejki.
     */
    this.prepend = function(song, skipRender) {
      if (!(song instanceof clipTime.Song)) {
        console.error(song, 'Argument is not an instance of clipTime.Song!');
      }
      queueList.unshift(song);

      if (!skipRender) {
        that.renderAll();
      }
    };

    /**
     * Dodaje podana piosenke na koniec kolejki.
     * @param  {clipTime.Song} song Piosenka do dodania.
     * @param  {Boolean} skipRender Czy odswiezyc widok HTML kolejki.
     */
    this.add = function(song, skipRender) {
      if (!(song instanceof clipTime.Song)) {
        console.error(song, 'Argument is not an instance of clipTime.Song!');
      }
      queueList.push(song);

      if (!skipRender) {
        that.renderAll();
      }
    };

    /**
     * Usuwa piosenke o podanym ID z kolejki i zwraca ja.
     * @param  {String} id ID piosenki do usuniecia.
     * @param  {Boolean} skipRender Czy odswiezyc widok HTML kolejki.
     * @return {clipTime.Song|null}    Usunieta piosenka lub null.
     */
    this.remove = function(id, skipRender) {
      for (var index = 0; index < queueList.length; index++) {
        if (queueList[index].id === id) {
          var removedSong = queueList[index];
          queueList.splice(index, 1);

          return removedSong;
        }
      }

      if (!skipRender) {
        that.renderAll();
      }

      return null;
    };

    /**
     * Usuwa pierwszy element z kolejki.
     * @param  {Boolean} skipRender Czy odswiezyc widok HTML kolejki.
     */
    this.shift = function(skipRender) {
      var shiftedSong = queueList.shift();
      if(shiftedSong) {
        mediator.publish('playedPrepend', shiftedSong);
      }

      if (!skipRender) {
        that.renderAll();
      }

      return null;
    };

    /**
     * Dodaje wszystkie piosenki z listy do kolejki odtwarzania.
     * @param  {[clipTime.Song]} songs Lista piosenek do dodania.
     */
    this.addAll = function(songs) {
      if (Object.prototype.toString.call(songs) !== '[object Array]') {
        console.error(songs, 'Argument is not an array!');
      }

      songs.forEach(function(index, song) {
        that.add(song, true);
      });

      that.renderAll();
    };

    /**
     * Generuje obiekt jQuery dla podanej piosenki.
     * @param  {clipTime.Song} song Piosenka.
     * @return {jQuery} Obiekt jQuery dla podanej piosenki.
     */
    this.render = function(song) {
      if (!(song instanceof clipTime.Song)) {
        console.error(song, 'Argument is not an instance of clipTime.Song!');
      }

      var $queueElement = $songPrototype.clone();
      $queueElement.attr('data-yt-video-id', song.id);
      $queueElement.find('.queue__song-title').text(song.title);
      $queueElement.find('.queue__song-author').text(song.channelTitle);

      return $queueElement;
    };

    /**
     * Generuje caly HTML aktualnej kolejki.
     */
    this.renderAll = function() {
      $queue.children('.queue__song, .queue__no-songs').remove();
      if (queueList.length > 0) {
        queueList.forEach(function(song) {
          $queue.append(that.render(song));
        });
        $queue.children('.queue__song').first().find('.queue__song-delete')
          .toggleClass('queue__song-delete queue__song-playing').find('i').text('music_note');
      } else {
        $queue.append('<li class="queue__no-songs">Queue is empty...</li>');
      }
    };

    /**
     * Ładuje wybrane wideo po kliknieciu na nie w liscie kolejki i przesuwa
     * go na jej przod.
     */
    $queue.on('click', '.queue__song', function(event) {
      var $queueSong = $(this).closest('.queue__song');

      if(!$queueSong.find('.queue__song-playing').length) {
        var id = $queueSong.attr('data-yt-video-id');
        mediator.publish('videoLoad', id);
        that.shift();
        that.prepend(that.remove(id, true));
      }
    });

    /**
     * Ususwa wybrane wideo z kolejki po kliknieciu na ikone usuwania.
     */
    $queue.on('click', '.queue__song-delete', function(event) {
      event.stopPropagation();
      var $queueSong = $(this).closest('.queue__song');
      var id = $queueSong.attr('data-yt-video-id');
      that.remove(id, true);
      $queueSong.fadeOut(200, function() {
        $queueSong.remove();
      });
    });

    mediator.subscribe('queueAdd', this.add);
    mediator.subscribe('queuePrepend', this.prepend);
    mediator.subscribe('queueNext', this.loadNext);
    mediator.subscribe('queueToggle', this.toggle);

    mediator.subscribe('queueHide', this.hide);
    mediator.subscribe('queueShow', this.show);

    mediator.subscribe('videoPlayed', this.shift);
    mediator.subscribe('videoEnded', this.loadNext);
  };

}(jQuery, window, clipTime));
