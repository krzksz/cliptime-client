(function($, clipTime){
  clipTime.youTube.Api = function() {
    var pendingRequest = null;

    this.search = function(query) {
      if(pendingRequest) {
        pendingRequest.abort();
        pendingRequest = null;
      }

      pendingRequest = $.ajax('https://www.googleapis.com/youtube/v3/search', {
          data: {
            key: 'AIzaSyA2GeAk3hx6pcyCbSnCp6_CrmEwTZzgtho ',
            type: 'video',
            maxResults: '16',
            part: 'id,snippet',
            videoCategoryId: 10,
            fields: 'items/id,items/snippet/title,items/snippet/description,items/snippet/thumbnails,items/snippet/channelTitle',
            q: query
          }
        });

      return pendingRequest;
    };
  };

}(jQuery, clipTime));
