/**
 * Wzorzec projektowy Adapter.
 * Pośredniczy w zamianie interfejsu YouTube API na interfejs naszego player'a,
 * w taki sposob, aby uodpornić naszą aplikację na zmiany w YouTube API.
 */

(function($, window, clipTime) {

  clipTime.youTube.Player = function(playerId) {
    /**
     * Obiekt YouTube player.
     */
    var ytPlayer;
    /**
     * Jeśli nie ma w html takiego elementu to błąd.
     */
    if($('#' + playerId).length === 0) {
      throw 'Cannot find element with id "' + playerId + '"!';
    }
    /**
     * Pobieram obiekt mediatora.
     */
    var mediator = clipTime.Mediator.getInstance();

    /**
     * Funkcja, ktora wywolujemy w eventlistenerze,
     * jesli zmieni sie stan okna odtwarzacza.
     */
    var onStateChange = function(state) {
      mediator.publish('video' + state);
    };

    window.onPlayerReady = function() {
      ytPlayer.addEventListener('onStateChange', function(event) {
        if (event.data == YT.PlayerState.PLAYING) {
          onStateChange('Playing');
        } else if (event.data == YT.PlayerState.PAUSED) {
          onStateChange('Paused');
        } else if (event.data == YT.PlayerState.ENDED) {
          onStateChange('Ended');
        }
      });
    };

    /**
     * Funkcja wywołuje się gdy załaduje się biblioteka z YT.
     */
    window.onYouTubeIframeAPIReady = function() {
      /**
       * Tworzę obiekt odtwarzacza, ktory będziemy adaptować.
       */
      ytPlayer = new YT.Player(playerId, {
        videoId: '',
        height: '720',
        width: '1280',
        playerVars: {
          color: 'white',
          autoplay: 0,
          enablejsapi: 1,
          rel: 0,
          iv_load_policy: 3,
          controls: 0
        },
        events: {
          'onReady': onPlayerReady
        }
      });
    };

    /**
     * Zatrzymuje odtwarzanie
     */
    this.stopVideo = function() {
      ytPlayer.stopVideo();
      mediator.publish('videoStopped');
    };

    /**
     * Pauzuje odtwarzanie.
     */
    this.pauseVideo = function() {
      ytPlayer.pauseVideo();
    };

    /**
     * Startuje odtwarzanie.
     */
    this.playVideo = function() {
      ytPlayer.playVideo();
    };

    /**
     * Laduje i uruchamia video o podanym identyfikatorze.
     */
    this.loadVideo = function(id) {
      ytPlayer.loadVideoById(id, 0, 'hd720');
      mediator.publish('videoLoaded');
      $('#' + playerId).css('visibility', 'visible');
    };

    /**
     * Pobiera dlugosc video.
     */
    this.getDuration = function() {
      return ytPlayer.getDuration();
    };

    /**
     * Pobiera aktualny czas odtwarzania.
     */
    this.getCurrentTime = function() {
      return ytPlayer.getCurrentTime();
    };

    /**
     * Przesuwa odtwarzanie do podanej sekundy
     */
    this.seekTo = function(seconds) {
      return ytPlayer.seekTo(seconds);
    };

    /**
     * Przypisuje referencje funkcji do komunikatu w mediatorze.
     */
    mediator.subscribe('videoStop', this.stopVideo);
    mediator.subscribe('videoPause', this.pauseVideo);
    mediator.subscribe('videoPlay', this.playVideo);
    mediator.subscribe('videoLoad', this.loadVideo);

  };

}(jQuery, window, clipTime));
