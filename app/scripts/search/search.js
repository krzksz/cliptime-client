/**
 *	wyszukiwanie piosenek za pośrednictwem Youtube API
 */

(function($, window, clipTime) {
  clipTime.search.Search = function(youTubeApi) {
	var that = this;

	/**
	 * wyodrębnienie części odpowiedzialnej za pole wyszukiwania
	 */
    var searchClass = '.search';

	/**
	 * obiekt jQuery pola wyszukiwania
	 */
    var $search = $(searchClass);

	/**
	 * sprawdzenie, czy przekazany argument jest odpowiedniego typu
	 */
    if(!(youTubeApi instanceof clipTime.youTube.Api)) {
      throw 'Search requires first parameter to be an instance of clipTime.youTube.Api!';
    }

	/**
	 * przypisanie do lokalnej zmiennej instancji obiektu Mediator
	 */
    var mediator = clipTime.Mediator.getInstance();

	/**
     * funkcja wypełnia tablicę foundSongs wynikami, które zwraca YouTube API,
	 * po czym tablica ta przekazywana jest od Mediatora, aby dodać ją do listy wyników do wyświetlenia
     */
    this.getResults = function(query) {
      youTubeApi.search(query).success(function(data) {
        mediator.publish('resultsClear');
        var foundSongs = [];
        data.items.forEach(function(item) {
          foundSongs.push(new clipTime.Song(item));
        });
        mediator.publish('resultsAddAll', foundSongs);
      });
    };

	/**
	 * zmienna lokalna odpowiadająca za timeout
	 */
    var searchTimeout;

	/**
	 * funkcja wywoływana pod wpływem pojawienia się znaków w polu wyszukiwania.
	 * Wywołuje ona funkcję getResults z argumentem query, jeżeli w polu wyszukiwania
	 * pojawią się conajmniej trzy nowe znaki i od pojawienia się ostatniego znaku
	 * minęło co najmniej 300 milisekund.
	 */
    $(searchClass + '__input').on('input', function() {
      var query = $(this).val();
      if (query.length > 2) {
        clearTimeout(searchTimeout);
        searchTimeout = setTimeout(function() {
          mediator.publish('queueHide');
          mediator.publish('playedHide');
          mediator.publish('resultsShow');
          that.getResults(query);
        }, 300);
      }
    });

	/**
	 * obiekty jQuery identyfikujące przycisk wyszukiwania (lupkę), ikonę tego przycisku
	 * oraz formularz, dzięki któremu możliwe będzie ukrycie/pokazanie przycisku
	 */
    var $searchButton = $('.search__button');
    var $searchIcon = $searchButton.find('.material-icons');
    var $searchForm = $('.search__form');

	/**
	 * funkcja odpowiadająca za zmianę widoku wyszukiwania/kolejki.
	 * Jeżeli wyświetlana jest lista wyszukiwania to po naciśnięciu przycisku
	 * "lupki" lista wyszukiwania jest wygaszana i pojawia się lista kolejki
	 */
    var toggleSearch = function( event ) {
      event.preventDefault();
      //$searchForm.find('.search__input').val('');
      if($searchIcon.text() === 'search') {
        mediator.publish('queueHide');
        mediator.publish('historyHide');
        mediator.publish('resultsShow');
        $searchForm.addClass('search__form--visible');
        $searchIcon.fadeOut(200, function() {
          $searchIcon.text('close').fadeIn();
        });
      } else {
        $searchForm.removeClass('search__form--visible');
        $searchIcon.fadeOut(200, function() {
          $searchIcon.text('search').fadeIn();
        });
        mediator.publish('resultsHide');
        mediator.publish('queueShow');
      }
    };
    $searchButton.on('click', toggleSearch);

  };

}(jQuery, window, clipTime));
