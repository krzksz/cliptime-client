(function($, window, clipTime) {

    clipTime.search.Results = function() {
      var that = this;
      var $results = $('.search__results');
      var $resultPrototype = $('<li class="search__result | collection-item avatar">' +
        '<div class="search__result-thumbnail"><img src="" alt=""></div>' +
        '<div class="search__result-details">' +
        '<span class="search__result-title"></span>' +
        '<p class="search__result-author"></p>' +
        '<p class="search__result-description"></p></div>' +
        '<a href="#" class="search__result-add | secondary-content"><i class="material-icons">add</i></a>' +
        '</li>');
      var resultsList = [];

      var mediator = clipTime.Mediator.getInstance();

      this.getElement = function() {
        return $results;
      };

      var toggleTimeout = null;

      this.hide = function() {
        clearTimeout(toggleTimeout);
        if($results.is(':visible')) {
          $results.fadeOut(200);
        }
      };

      this.show = function() {
        clearTimeout(toggleTimeout);
        if(!$results.is(':visible')) {
          toggleTimeout = setTimeout(function() {
            $results.fadeIn(200);
          }, 200);
        }
      };

      this.clear = function() {
        resultsList = [];
      };

      this.get = function(id) {
        for (var index = 0; index < resultsList.length; index++) {
          if (resultsList[index].id === id) {
            return resultsList[index];
          }
        }

        return null;
      };

      this.getAll = function() {
        return resultsList;
      };

      this.add = function(song, skipRender) {
        if (!(song instanceof clipTime.Song)) {
          throw 'First argument must be an instance of clipTime.Song!';
      }

      resultsList.push(song);
      if (!skipRender) {
        that.renderAll();
      }
    };

    this.addAll = function(songs) {
      if (Object.prototype.toString.call(songs) !== '[object Array]') {
        throw 'First argument is not an array!';
    }

    songs.forEach(function(song) {
      that.add(song, true);
    });

    that.renderAll();
  };

  this.render = function(song) {
    if (!(song instanceof clipTime.Song)) {
      console.warn(songs + ' is not an instance of clipTime.Song!');
      return;
    }

    var $resultElement = $resultPrototype.clone();
    $resultElement.attr('data-yt-video-id', song.id);
    $resultElement.find('.search__result-title').text(song.title);
    $resultElement.find('.search__result-author').text(song.channelTitle);
    $resultElement.find('.search__result-description').text(song.description);
    $resultElement.find('.search__result-thumbnail img').attr('src', song.thumbnails.medium.url);

    return $resultElement;
  };

  this.renderAll = function() {
    $results.children('.search__result, .search__no-results').remove();
    if (resultsList.length > 0) {
      resultsList.forEach(function(song) {
        $results.append(that.render(song));
      });
    } else {
      $results.append('<li class="search__no-results">No results found...</li>');
    }
  };

  $results.on('click', '.search__result', function(event) {
    var videoId = $(this).attr('data-yt-video-id');
    mediator.publish('videoLoad', videoId);
    mediator.publish('queuePrepend', that.get(videoId));
  });

  $results.on('click', '.search__result-add', function(event) {
    event.preventDefault();
    event.stopPropagation();
    var id = $(this).closest('.search__result').attr('data-yt-video-id');
    mediator.publish('queueAdd', that.get(id));
  });

  mediator.subscribe('resultsClear', this.clear);
  mediator.subscribe('resultsAddAll', this.addAll);

  mediator.subscribe('resultsHide', this.hide);
  mediator.subscribe('resultsShow', this.show);
};

}(jQuery, window, clipTime));
