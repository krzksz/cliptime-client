(function($, window, clipTime) {
  clipTime.Controls = function(youTubePlayer) {
    if(!(youTubePlayer instanceof clipTime.youTube.Player)) {
      throw 'Controls requires first parameter to be an instance of clipTime.youTube.Player!';
    }

    var that = this;
    var controlsClass = '.controls';
    var $controls = $(controlsClass);
    var $controlsProgress = $(controlsClass + '__progress');
    var $controlsPlay = $(controlsClass + '__play');
    var videoLoaded = false;

    var mediator = clipTime.Mediator.getInstance();


    var progressInterval = null;
    var updateProgress = function() {
      progressInterval = setInterval(function() {
        $controlsProgress.children('.determinate')
          .css('width', youTubePlayer.getCurrentTime() * 100 / youTubePlayer.getDuration() + '%');
      }, 1000);
    };

    var stopProgress = function() {
      clearInterval(progressInterval);
    };

    var setPlay = function() {
      if($controlsPlay.children('i').text() === 'pause') {
        $controlsPlay.children('i').text('play_arrow');
      }
    };

    var setPause = function() {
      if($controlsPlay.children('i').text() === 'play_arrow') {
        $controlsPlay.children('i').text('pause');
      }
    };

    var togglePlayback = function(event) {
      event.preventDefault();
      if(!videoLoaded) {
        return;
      }

      if($controlsPlay.children('i').text() === 'play_arrow') {
        setPause();
        mediator.publish('videoPlay');
      } else {
        setPlay();
        mediator.publish('videoPause');
      }
    };

    $controls.find(controlsClass + '__next').on('click', function(event) {
      event.preventDefault();
      mediator.publish('queueNext');
    });

    $controls.find(controlsClass + '__previous').on('click', function(event) {
      event.preventDefault();
      mediator.publish('playedLoad');
    });



    $controls.find(controlsClass + '__toggle').on('click', function(event) {
      event.preventDefault();
      var $controlIcon = $(this).children('i');
      if($controlIcon.text() === 'schedule') {
        mediator.publish('queueHide');
        mediator.publish('resultsHide');
        mediator.publish('playedShow');
        $controlIcon.text('list');
      } else {
        mediator.publish('playedHide');
        mediator.publish('queueShow');
        $controlIcon.text('schedule');
      }
    });
    $controls.find(controlsClass + '__play').on('click', togglePlayback);

    $controlsProgress.on('click', function(e) {
        var offset = $controlsProgress.offset();
        youTubePlayer.seekTo(youTubePlayer.getDuration() * (e.pageX - offset.left) / $controlsProgress.width());
    });

    mediator.subscribe('videoPlaying', updateProgress);
    mediator.subscribe('videoPaused', stopProgress);
    mediator.subscribe('videoPaused', setPlay);
    mediator.subscribe('videoStopped', stopProgress);
    mediator.subscribe('videoStopped', setPause);
    mediator.subscribe('videoLoaded', setPause);
    mediator.subscribe('videoLoaded', function() {
      videoLoaded = true;
    });
  };

}(jQuery, window, clipTime));
