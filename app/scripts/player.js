(function($, window, clipTime){
  clipTime.Player = function() {

    this.init = function() {
      var search = new clipTime.search.Search(new clipTime.youTube.Api());
      var searchResults = new clipTime.search.Results();
      searchResults.renderAll();

      var ytPlayer = new clipTime.youTube.Player('video__iframe');

      var played = new clipTime.Played();
      played.renderAll();
      var queue = new clipTime.Queue();
      queue.renderAll();

      var controls = new clipTime.Controls(ytPlayer);
    };
  };

}(jQuery, window, clipTime));
