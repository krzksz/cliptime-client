/**
 * Mediator wzorzec projektowy - zmiana relacji wiele-wiele na jeden-wiele
 * żeby obiekty search, queue, player, controls,results nie musiały się komunikować między sobą, nie muszą nawet wiedzieć o swoim istnieniu,
 * wysyłają tylko te komunikaty do Mediatora lub je wywołują
 */

(function(window, clipTime) {

  clipTime.Mediator = function() {

    /**
    * Obiekt MediatorConstructor - wewnętrzny obiekt którego instancje będziemy tworzyć w singletonie
    */

    var MediatorConstructor = function() {
      /**
      * Lista mediatora [{nazwa_zdarzenia,funkcje}]
      */
      var topics = {};

      /**
      * Dodaje do listy zdarzeń mediatora zdarzenie i referencje funkcji jaka ma być wywołana w związku z tym zdarzeniem
	  * jeśli nie ma takiego zdarzenia, to tworzy nowy
	  * jeśli jest, to dopisuje do listy funkcji zdarzenia tą kolejną referencje funkcji
      */

      this.subscribe = function(topic, callback) {
        console.log('Subscribe', topic, callback);
        if (!topics.hasOwnProperty(topic)) {
          topics[topic] = [];
        }
        topics[topic].push(callback);

        return true;
      };

      /**
      * Usuwa z listy zdarzeń mediatora zdarzenie i referencje funkcji
	  * jeśli nie ma takiego zdarzenia, to nic nie robi
	  * jeśli jest, to usuwa xd
      */

      this.unsubscribe = function(topic, collback) {
        if (!topics.hasOwnProperty(topic)) {
          return false;
        }

        for (var i = 0, len = topics[topic].length; i < len; i++) {
          if (topics[topic][i] === callback) {
            topics[topic].splice(i, 1);
            return true;
          }
        }

        return false;
      };

      /**
      * Wywołuje funkcje, które są przyporządkowane do zadanego zdarzenia
	  * z argumentami które były podane razem z zadanym zdarzeniem
      */

      this.publish = function() {
        var args = Array.prototype.slice.call(arguments);
        var topic = args.shift();
        console.log('Publish', topic);

        if (!topics.hasOwnProperty(topic)) {
          return false;
        }

        for (var i = 0, len = topics[topic].length; i < len; i++) {

          topics[topic][i].apply(undefined, args);
        }
        return true;
      };
    };

    /**
    * Obiekt instance - przechowuje instancje MediatorControler - singletona
    */

    var instance = null;

    /**
    * Zwraca instancje MediatorControlera, jeśli jest nullem to go tworzy
    */

    function getInstance() {
      if (!instance) {
        instance = new MediatorConstructor();
      }

      return instance;
    }

    return {
      getInstance: getInstance
    };

  }();

})(window, clipTime);
